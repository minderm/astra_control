<?php
    function make_input_url_short($input_id)
    {
        if(!$input_id) return;
        $query = new db_query();
        $input_cfg = $query->assoc_array("select input.*, dvb_input.name from input
            left join dvb_input using(dvb_input_id)
            where input_id=".$input_id);

        switch ($input_cfg['type'])
        {
            case 'dvb':
                $input = 'dvb://dvb_'.$input_cfg['dvb_input_id']." (".$input_cfg['name'].")";
                break;
            case 'file':
                #file://path
                $input="file://".$input_cfg['path'];
                break;
            case 'udp':
                #udp://interface@address:port
                $input = 'udp://@'.$input_cfg['address'].':'.$input_cfg['port'];
                break;
            case 'http':
                #http://login:password@host:port/uri
                $input = $input_cfg['address'].':'.$input_cfg['port'];
                if( $input_cfg['path'][0]=='/' ) $input_cfg['path'] = substr($input_cfg['path'],1);
                $input = 'http://'.$input.'/'.$input_cfg['path'];
                break;
        }

        return $input;
    }

    function make_output_url_short($output_id){

        if(!$output_id) return;  
        $query = new db_query();
        $output='';
        $output_cfg = $query->assoc_array("select * from output
            where output_id=".$output_id);

        switch ($output_cfg['type'])
        {
            case 'udp':
                $output = $output_cfg['address'].':'.$output_cfg['port'];

                if ($output_cfg['rtp'] == 'true')
                {
                    $output = 'rtp://@'.$output;    
                    $separator = '&';
                }else{
                    $output = 'udp://@'.$output;    
                    $separator = '&';
                }
                break;
            case 'file':
                $output = 'file://'.$output_cfg['path'];
                break;
            case 'http:':
                $output = $output_cfg['interface'].':'.$output_cfg['port'];
                if( $output_cfg['path'][0]=='/' ) $output_cfg['path'] = substr($output_cfg['path'],1 );
                $output = 'http://'.$output.'/'.$output_cfg['path'];

                break;
        }
        return $output;
    }


    function make_input_url($input_id, $channel_cfg = array('channel_pnr' => 0 ))
    {
        if(!$input_id) return; 

        $query = new db_query();
        $filter_request = new db_query();

        $input_cfg = $query->assoc_array("select input.*, dvb_input.no_sdt, dvb_input.no_eit
            from input left join dvb_input using(dvb_input_id)
            where input_id=".$input_id);
        //INPUT
        $separator = '#';
        switch ($input_cfg['type'])
        {
            case 'dvb':
                $input = 'dvb://dvb_'.$input_cfg['dvb_input_id'];
                if ($input_cfg['no_sdt'] == 'true')
                {
                    $input = $input.$separator.'no_sdt';
                    $separator = '&';
                }
                if ($input_cfg['no_eit'] == 'true')
                {
                    $input = $input.$separator.'no_eit';
                    $separator = '&';
                }
                break;
            case 'file':
                #file://path
                $input="file://".$input_cfg['path'];
                if ($input_cfg['loop'] == 'true')
                {
                    $input = $input.$separator.'loop';
                    $separator = '&';
                }
                if ($input_cfg['lock'])
                {
                    $input = $input.$separator.'lock='.$input_cfg['lock'];
                    $separator = '&';
                }
                if ($input_cfg['buffer_size'])
                {
                    $input = $input.$separator.'buffer_size='.$input_cfg['buffer_size'];
                    $separator = '&';
                }

                break;
            case 'udp':
                #udp://interface@address:port
                $input = $input_cfg['address'].':'.$input_cfg['port'];
                if ($input_cfg['interface'])
                {
                    $input = $input_cfg['interface'].'@'.$input;
                }
                if ($input_cfg['renew'])
                {
                    $input = $input.$separator.'renew='.$input_cfg['renew'];
                    $separator = '&';
                }
                if ($input_cfg['socket_size'])
                {
                    $input = $input.$separator.'socket_size='.$input_cfg['socket_size'];
                    $separator = '&';
                }
                $input = 'udp://'.$input;
                break;
            case 'http':
                #http://login:password@host:port/uri
                $input = $input_cfg['address'].':'.$input_cfg['port'];
                if ($input_cfg['login'] && $input_cfg['password'])
                {
                    $input = $input_cfg['login'].':'.$input_cfg['password'].'@'.$input;
                }
                if ($input_cfg['sync' == 'true'])
                {
                    $input = $input.$separator.'sync';
                    $separator = '&';
                }

                if( $input_cfg['path'][0]=='/' ) $input_cfg['path'] = substr($input_cfg['path'],1);
                $input = 'http://'.$input.'/'.$input_cfg['path'];
                break;
        }
        $input = $input.$separator.'pnr='.$input_cfg['pnr'];
        $separator = '&';

        if($input_cfg['filter']!=''){
            $input = $input.$separator.'filter='.$input_cfg['filter'];
        }

        if($input_cfg['shift']){
            $input = $input.$separator.'shift='.$input_cfg['shift'];
        }

        if($input_cfg['no_analyze'] == 'true' ){
            $input = $input.$separator.'no_analyze';
        }

        //set_pnr
        if($channel_cfg['channel_pnr'] && !$input_cfg['set_pnr']){
            $input_cfg['set_pnr'] = $channel_cfg['channel_pnr'];
        }

        if($input_cfg['set_pnr']){
            $input = $input.$separator.'set_pnr='.$input_cfg['set_pnr'];
        }

        //
        if($input_cfg['biss'] && $input_cfg['cam'] == 'biss') {
            $input = $input.$separator.'biss='.$input_cfg['biss'];
        }
        if($input_cfg['cam'] == 'hardware') {
            $input = $input.$separator.'cam';
        }
        if($input_cfg['newcamd_id'] && $input_cfg['cam'] == 'newcamd' ) {
            $input = $input.$separator.'cam=cam_'.$input_cfg['newcamd_id'];
            if ($input_cfg['cas_data'])
            {
                $input = $input.$separator.'cas_data='.$input_cfg['cas_data'];
            }
        }

        $input=$input.$separator.'id='.$input_cfg['input_id'];

        return $input;
    }

    function make_output_url($output_id){

        if(!$output_id) return;    
        $query = new db_query();
        $output = '';
        $output_cfg = $query->assoc_array("select * from output
            where output_id=".$output_id);
        $separator = '#';
        switch ($output_cfg['type'])
        {
            case 'udp':
                $output = $output_cfg['address'].':'.$output_cfg['port'];
                if ($output_cfg['interface'])
                {
                    $output = $output_cfg['interface'].'@'.$output;
                }

                if ($output_cfg['ttl'])
                {
                    $output = $output.$separator.'ttl='.$output_cfg['ttl'];
                    $separator = '&';
                }
                if ($output_cfg['socket_size'])
                {
                    $output = $output.$separator.'socket_size='.$output_cfg['socket_size'];
                    $separator = '&';
                }

                if ($output_cfg['rtp'] == 'true')
                {
                    $output = 'rtp://'.$output;
                }else{
                    $output = 'udp://'.$output;                    
                }

                break;
            case 'file':
                $output = 'file://'.$output_cfg['path'];
                if ($output_cfg['m2ts'] == 'true')
                {
                    $output = $output.$separator.'m2ts';
                    $separator = '&';
                }
                break;
            case 'http':
                $output = $output_cfg['interface'].':'.$output_cfg['port'];

                if ($output_cfg['keep_active'] == 'true')
                {
                    $output = $output.$separator.'keep_active';
                    $separator = '&';
                }

                if( $output_cfg['path'][0]=='/' ) $output_cfg['path'] = substr($output_cfg['path'],1 );
                $output = 'http://'.$output.'/'.$output_cfg['path'];
                break;
        }

        $mixaudio_request = new db_query();
        $mixaudio_request->result("select * from mixaudio where output_id='".$output_cfg['output_id']."'");
        $mixaudio='';
        while (is_array($mixaudio_cfg=$mixaudio_request->fetch_assoc()))
        {
            $mixaudio[]='mixaudio='.$mixaudio_cfg['pid'].'/'.$mixaudio_cfg['mode'];
        }
        if (is_array($mixaudio))
        {
            $mixaudio = implode('&',$mixaudio);
            $output=$output.$separator.$mixaudio;
        }

        if($output_cfg['biss']) {
            $output = $output.$separator.'biss='.$output_cfg['biss'];
        }

        if($output_cfg['sync'] == 'true' ) {
            $output = $output.$separator.'sync';
        }

        if($output_cfg['cbr'] > 0 ) {
            $output = $output.$separator.'cbr='.$output_cfg['cbr'];
        }


        return $output;        
    }

?>
